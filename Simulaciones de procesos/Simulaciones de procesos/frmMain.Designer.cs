﻿
namespace Simulaciones_de_procesos
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.rbtnInicio = new System.Windows.Forms.RadioButton();
            this.rbtnPuertaAutoclave = new System.Windows.Forms.RadioButton();
            this.rbtnTanque = new System.Windows.Forms.RadioButton();
            this.rbtnHorno = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ucHorno1 = new Simulaciones_de_procesos.ucHorno();
            this.ucTanque1 = new Simulaciones_de_procesos.ucTanque();
            this.ucPuertaAutoclave1 = new Simulaciones_de_procesos.ucPuertaAutoclave();
            this.panelInicio = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPuertaAutoclave = new System.Windows.Forms.Button();
            this.btnTanque = new System.Windows.Forms.Button();
            this.btnHorno = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panelInicio.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 152F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.rbtnInicio, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.rbtnPuertaAutoclave, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.rbtnTanque, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.rbtnHorno, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(851, 508);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.LemonChiffon;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::Simulaciones_de_procesos.Properties.Resources.Embotellado_garrafas;
            this.pictureBox1.Location = new System.Drawing.Point(5, 5);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(142, 130);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // rbtnInicio
            // 
            this.rbtnInicio.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnInicio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnInicio.FlatAppearance.BorderColor = System.Drawing.Color.LemonChiffon;
            this.rbtnInicio.FlatAppearance.BorderSize = 2;
            this.rbtnInicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnInicio.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnInicio.ForeColor = System.Drawing.Color.LemonChiffon;
            this.rbtnInicio.Image = global::Simulaciones_de_procesos.Properties.Resources.home;
            this.rbtnInicio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtnInicio.Location = new System.Drawing.Point(5, 145);
            this.rbtnInicio.Margin = new System.Windows.Forms.Padding(5);
            this.rbtnInicio.Name = "rbtnInicio";
            this.rbtnInicio.Size = new System.Drawing.Size(142, 45);
            this.rbtnInicio.TabIndex = 1;
            this.rbtnInicio.TabStop = true;
            this.rbtnInicio.Text = "          Inicio";
            this.rbtnInicio.UseVisualStyleBackColor = true;
            this.rbtnInicio.CheckedChanged += new System.EventHandler(this.rbtnInicio_CheckedChanged);
            // 
            // rbtnPuertaAutoclave
            // 
            this.rbtnPuertaAutoclave.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnPuertaAutoclave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnPuertaAutoclave.FlatAppearance.BorderColor = System.Drawing.Color.LemonChiffon;
            this.rbtnPuertaAutoclave.FlatAppearance.BorderSize = 2;
            this.rbtnPuertaAutoclave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnPuertaAutoclave.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnPuertaAutoclave.ForeColor = System.Drawing.Color.LemonChiffon;
            this.rbtnPuertaAutoclave.Image = global::Simulaciones_de_procesos.Properties.Resources.automatic_doors;
            this.rbtnPuertaAutoclave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtnPuertaAutoclave.Location = new System.Drawing.Point(5, 200);
            this.rbtnPuertaAutoclave.Margin = new System.Windows.Forms.Padding(5);
            this.rbtnPuertaAutoclave.Name = "rbtnPuertaAutoclave";
            this.rbtnPuertaAutoclave.Size = new System.Drawing.Size(142, 45);
            this.rbtnPuertaAutoclave.TabIndex = 1;
            this.rbtnPuertaAutoclave.TabStop = true;
            this.rbtnPuertaAutoclave.Text = "          Puerta de\r\n          autoclave";
            this.rbtnPuertaAutoclave.UseVisualStyleBackColor = true;
            this.rbtnPuertaAutoclave.CheckedChanged += new System.EventHandler(this.rbtnPuertaAutoclave_CheckedChanged);
            // 
            // rbtnTanque
            // 
            this.rbtnTanque.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTanque.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnTanque.FlatAppearance.BorderColor = System.Drawing.Color.LemonChiffon;
            this.rbtnTanque.FlatAppearance.BorderSize = 2;
            this.rbtnTanque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTanque.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTanque.ForeColor = System.Drawing.Color.LemonChiffon;
            this.rbtnTanque.Image = global::Simulaciones_de_procesos.Properties.Resources.industry_tank;
            this.rbtnTanque.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtnTanque.Location = new System.Drawing.Point(5, 255);
            this.rbtnTanque.Margin = new System.Windows.Forms.Padding(5);
            this.rbtnTanque.Name = "rbtnTanque";
            this.rbtnTanque.Size = new System.Drawing.Size(142, 45);
            this.rbtnTanque.TabIndex = 1;
            this.rbtnTanque.TabStop = true;
            this.rbtnTanque.Text = "          Nivel de\r\n          tanque";
            this.rbtnTanque.UseVisualStyleBackColor = true;
            this.rbtnTanque.CheckedChanged += new System.EventHandler(this.rbtnTanque_CheckedChanged);
            // 
            // rbtnHorno
            // 
            this.rbtnHorno.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnHorno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtnHorno.FlatAppearance.BorderColor = System.Drawing.Color.LemonChiffon;
            this.rbtnHorno.FlatAppearance.BorderSize = 2;
            this.rbtnHorno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnHorno.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnHorno.ForeColor = System.Drawing.Color.LemonChiffon;
            this.rbtnHorno.Image = global::Simulaciones_de_procesos.Properties.Resources.horno;
            this.rbtnHorno.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtnHorno.Location = new System.Drawing.Point(5, 310);
            this.rbtnHorno.Margin = new System.Windows.Forms.Padding(5);
            this.rbtnHorno.Name = "rbtnHorno";
            this.rbtnHorno.Size = new System.Drawing.Size(142, 45);
            this.rbtnHorno.TabIndex = 1;
            this.rbtnHorno.TabStop = true;
            this.rbtnHorno.Text = "          Temperatura\r\n          de horno";
            this.rbtnHorno.UseVisualStyleBackColor = true;
            this.rbtnHorno.CheckedChanged += new System.EventHandler(this.rbtnHorno_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LemonChiffon;
            this.panel1.Controls.Add(this.panelInicio);
            this.panel1.Controls.Add(this.ucPuertaAutoclave1);
            this.panel1.Controls.Add(this.ucHorno1);
            this.panel1.Controls.Add(this.ucTanque1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(152, 5);
            this.panel1.Margin = new System.Windows.Forms.Padding(0, 5, 5, 5);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 7);
            this.panel1.Size = new System.Drawing.Size(694, 498);
            this.panel1.TabIndex = 2;
            // 
            // ucHorno1
            // 
            this.ucHorno1.BackColor = System.Drawing.Color.Transparent;
            this.ucHorno1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucHorno1.Location = new System.Drawing.Point(0, 0);
            this.ucHorno1.Name = "ucHorno1";
            this.ucHorno1.Size = new System.Drawing.Size(694, 498);
            this.ucHorno1.TabIndex = 2;
            this.ucHorno1.Visible = false;
            // 
            // ucTanque1
            // 
            this.ucTanque1.BackColor = System.Drawing.Color.Transparent;
            this.ucTanque1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucTanque1.Location = new System.Drawing.Point(0, 0);
            this.ucTanque1.Name = "ucTanque1";
            this.ucTanque1.Size = new System.Drawing.Size(694, 498);
            this.ucTanque1.TabIndex = 1;
            this.ucTanque1.Visible = false;
            // 
            // ucPuertaAutoclave1
            // 
            this.ucPuertaAutoclave1.BackColor = System.Drawing.Color.Transparent;
            this.ucPuertaAutoclave1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPuertaAutoclave1.Location = new System.Drawing.Point(0, 0);
            this.ucPuertaAutoclave1.Name = "ucPuertaAutoclave1";
            this.ucPuertaAutoclave1.Size = new System.Drawing.Size(694, 498);
            this.ucPuertaAutoclave1.TabIndex = 0;
            this.ucPuertaAutoclave1.Visible = false;
            // 
            // panelInicio
            // 
            this.panelInicio.Controls.Add(this.tableLayoutPanel2);
            this.panelInicio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInicio.Location = new System.Drawing.Point(0, 0);
            this.panelInicio.Name = "panelInicio";
            this.panelInicio.Size = new System.Drawing.Size(694, 498);
            this.panelInicio.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.pictureBox2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(694, 498);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.LemonChiffon;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Image = global::Simulaciones_de_procesos.Properties.Resources.Embotellado_garrafas;
            this.pictureBox2.Location = new System.Drawing.Point(10, 59);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(674, 290);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.btnPuertaAutoclave, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnTanque, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnHorno, 3, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 352);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(688, 55);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // btnPuertaAutoclave
            // 
            this.btnPuertaAutoclave.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.btnPuertaAutoclave.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(85)))), ((int)(((byte)(7)))));
            this.btnPuertaAutoclave.FlatAppearance.BorderSize = 3;
            this.btnPuertaAutoclave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPuertaAutoclave.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnPuertaAutoclave.ForeColor = System.Drawing.Color.LemonChiffon;
            this.btnPuertaAutoclave.Image = global::Simulaciones_de_procesos.Properties.Resources.automatic_doors;
            this.btnPuertaAutoclave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPuertaAutoclave.Location = new System.Drawing.Point(124, 5);
            this.btnPuertaAutoclave.Margin = new System.Windows.Forms.Padding(5);
            this.btnPuertaAutoclave.Name = "btnPuertaAutoclave";
            this.btnPuertaAutoclave.Size = new System.Drawing.Size(140, 45);
            this.btnPuertaAutoclave.TabIndex = 0;
            this.btnPuertaAutoclave.Text = "          Puerta de\r\n          autoclave\r\n";
            this.btnPuertaAutoclave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPuertaAutoclave.UseVisualStyleBackColor = false;
            this.btnPuertaAutoclave.Click += new System.EventHandler(this.btnPuertaAutoclave_Click);
            // 
            // btnTanque
            // 
            this.btnTanque.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.btnTanque.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(85)))), ((int)(((byte)(7)))));
            this.btnTanque.FlatAppearance.BorderSize = 3;
            this.btnTanque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTanque.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnTanque.ForeColor = System.Drawing.Color.LemonChiffon;
            this.btnTanque.Image = global::Simulaciones_de_procesos.Properties.Resources.industry_tank;
            this.btnTanque.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTanque.Location = new System.Drawing.Point(274, 5);
            this.btnTanque.Margin = new System.Windows.Forms.Padding(5);
            this.btnTanque.Name = "btnTanque";
            this.btnTanque.Size = new System.Drawing.Size(140, 45);
            this.btnTanque.TabIndex = 0;
            this.btnTanque.Text = "          Nivel de\r\n          tanque";
            this.btnTanque.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTanque.UseVisualStyleBackColor = false;
            this.btnTanque.Click += new System.EventHandler(this.btnTanque_Click);
            // 
            // btnHorno
            // 
            this.btnHorno.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.btnHorno.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(85)))), ((int)(((byte)(7)))));
            this.btnHorno.FlatAppearance.BorderSize = 3;
            this.btnHorno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHorno.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnHorno.ForeColor = System.Drawing.Color.LemonChiffon;
            this.btnHorno.Image = global::Simulaciones_de_procesos.Properties.Resources.horno;
            this.btnHorno.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHorno.Location = new System.Drawing.Point(424, 5);
            this.btnHorno.Margin = new System.Windows.Forms.Padding(5);
            this.btnHorno.Name = "btnHorno";
            this.btnHorno.Size = new System.Drawing.Size(140, 45);
            this.btnHorno.TabIndex = 0;
            this.btnHorno.Text = "          Temperatura\r\n          de horno\r\n";
            this.btnHorno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHorno.UseVisualStyleBackColor = false;
            this.btnHorno.Click += new System.EventHandler(this.btnHorno_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.ClientSize = new System.Drawing.Size(851, 508);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "frmMain";
            this.Text = "Simulación de Procesos Industriales";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panelInicio.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton rbtnInicio;
        private System.Windows.Forms.RadioButton rbtnPuertaAutoclave;
        private System.Windows.Forms.RadioButton rbtnTanque;
        private System.Windows.Forms.RadioButton rbtnHorno;
        private System.Windows.Forms.Panel panel1;
        private ucPuertaAutoclave ucPuertaAutoclave1;
        private ucTanque ucTanque1;
        private ucHorno ucHorno1;
        private System.Windows.Forms.Panel panelInicio;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnPuertaAutoclave;
        private System.Windows.Forms.Button btnTanque;
        private System.Windows.Forms.Button btnHorno;
    }
}