﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulaciones_de_procesos
{
    public partial class ucPuertaAutoclave : UserControl
    {
        public ucPuertaAutoclave()
        {
            InitializeComponent();
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            tmrAbrir.Enabled = true;
            tmrCerrar.Enabled = false;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            tmrCerrar.Enabled = true;
            tmrAbrir.Enabled = false;
        }

        private void tmrAbrir_Tick(object sender, EventArgs e)
        {
            // Este es el evento del botón de abrir la puerta
            if (panelPuerta.Height >= 5)
            {
                panelPuerta.Height = panelPuerta.Height - 5; // decrementa la altura en 5 pixeles
                panelPuerta.Top = panelPuerta.Top + 5; // baja la puerta 5 pixeles
            }
            else
            {
                tmrAbrir.Enabled = false;
            }
        }

        private void tmrCerrar_Tick(object sender, EventArgs e)
        {
            /* Este es el evento del botón de cerrar la puerta.
             * Lo que hace es incrementar la altura de la puerta en 5 pixeles y 
             * subirla en el eje Y 5 pixeles también
             */
            if (panelPuerta.Height <= 160)
            {
                panelPuerta.Height = panelPuerta.Height + 5;
                panelPuerta.Top = panelPuerta.Top - 5;
            }
            else
            {
                tmrCerrar.Enabled = false;
            }
        }
    }
}
