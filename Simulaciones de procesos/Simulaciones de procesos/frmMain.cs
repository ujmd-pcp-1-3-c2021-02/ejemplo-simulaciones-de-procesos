﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulaciones_de_procesos
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void rbtnPuertaAutoclave_CheckedChanged(object sender, EventArgs e)
        {
            // Forma larga de cambiar visibilidad
            if (rbtnPuertaAutoclave.Checked)
            {
                ucPuertaAutoclave1.Visible = true;
            }
            else
            {
                ucPuertaAutoclave1.Visible = false;
            }
        }

        private void rbtnTanque_CheckedChanged(object sender, EventArgs e)
        {
            // Forma corta de cambiar visibilidad
            ucTanque1.Visible = rbtnTanque.Checked;
        }

        private void rbtnHorno_CheckedChanged(object sender, EventArgs e)
        {
            ucHorno1.Visible = rbtnHorno.Checked;
        }

        private void rbtnInicio_CheckedChanged(object sender, EventArgs e)
        {
            panelInicio.Visible = rbtnInicio.Checked;
        }

        private void btnPuertaAutoclave_Click(object sender, EventArgs e)
        {
            rbtnPuertaAutoclave.Checked = true;
        }

        private void btnTanque_Click(object sender, EventArgs e)
        {
            rbtnTanque.Checked = true;
        }

        private void btnHorno_Click(object sender, EventArgs e)
        {
            rbtnHorno.Checked = true;
        }
    }
}
